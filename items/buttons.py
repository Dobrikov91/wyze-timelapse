from kivy.lang import Builder
from kivymd.uix.behaviors.toggle_behavior import MDToggleButton
from kivymd.uix.button import MDRectangleFlatButton

Builder.load_string('''
<ConfigToggleButton>:
    name: ''
    section: ''
    text: ''
    height: dp(36)
    size_hint_y: None

    state: 'down' if self.name and self.section and app.config.get(self.section, self.name) == 'True' else 'normal'
    on_release: app.config.set(self.section, self.name, True if self.state == 'down' else False)
''')

class SimpleToggleButton(MDRectangleFlatButton, MDToggleButton):
    pass

class ConfigToggleButton(MDRectangleFlatButton, MDToggleButton):
    pass
