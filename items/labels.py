from datetime import datetime

from kivy.lang import Builder
from kivy.properties import ObjectProperty, NumericProperty
from kivymd.uix.label import MDLabel

Builder.load_string('''
<TimeLabel>:
    text: root.time.strftime('%dd%H:%M:%S') if root.time else '00 00:00:00'
    size_hint_x: None
    width: dp(90)
    halign: 'center'

<FrameStepLabel>:
    text: '{:.2f} s'.format(root.step) if root.step < 60 else '{:.2f} m'.format(root.step / 60)
    size_hint_x: None
    width: dp(60)
    halign: 'center'
''')

class TimeLabel(MDLabel):
    time = ObjectProperty(datetime(1900, 1, 1))

class FrameStepLabel(MDLabel):
    step = NumericProperty(0)
