from kivymd.uix.dialog import MDDialog
from kivy.lang import Builder
from kivy.properties import StringProperty
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.button import MDRectangleFlatButton

Builder.load_string('''
<RecordedView>:
    orientation: 'vertical'
    size_hint_y: None
    height: dp(3*36)

    MDLabel:
        text: 'File name: '

    MDLabel:
        text: root.fileName

    MDLabel:
        text: 'Length: ' + root.length
''')

class RecordedView(MDBoxLayout):
    fileName = StringProperty('')
    length = StringProperty('')

class RecordedDialog(MDDialog):
    def __init__(self, **kwargs):
        super().__init__(
            title='Recording finished',
            type='custom',
            content_cls=RecordedView(),
            buttons=[
                MDRectangleFlatButton(
                    text='CLOSE',
                    on_release=lambda x: self.dismiss()),
            ],
        )

    def open(self, fileName, length):
        self.content_cls.fileName = fileName
        self.content_cls.length = length
        super().open()