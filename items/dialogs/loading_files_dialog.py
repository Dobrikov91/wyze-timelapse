from kivy.app import App
from kivy.lang import Builder
from kivy.properties import NumericProperty
from kivymd.uix.button import MDRectangleFlatButton
from kivymd.uix.dialog import MDDialog
from kivymd.uix.floatlayout import MDFloatLayout
from kivymd.uix.label import MDLabel
from kivymd.uix.progressbar import MDProgressBar

Builder.load_string('''
<LoadingFilesView>:
    orientation: 'horizontal'
    size_hint_y: None
    height: dp(24)

    MDProgressBar:
        value: 100 * root.fileId / root.totalFiles
        pos_hint: {'center_x': .5, 'center_y': .5}

    MDLabel:
        text: '{} / {}'.format(root.fileId, root.totalFiles)
        halign: 'center'
        pos_hint: {'center_x': .5, 'center_y': .5}

''')

class LoadingFilesView(MDFloatLayout):
    fileId = NumericProperty(0)
    totalFiles = NumericProperty(1)

class LoadingFilesDialog(MDDialog):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.dialog = None

    def open(self):
        if self.dialog is None:
            self.dialog = MDDialog(
                type='custom',
                title='Loading files...',
                auto_dismiss=False,
                content_cls=LoadingFilesView(),
                buttons=[
                    MDRectangleFlatButton(
                        text='CANCEL',
                        on_release=lambda x: App.get_running_app().controller.cancelLoadingFiles()),
                ],
            )
        self.updateProgress(0, 1)
        self.dialog.open()

    def dismiss(self):
        if self.dialog is not None:
            self.dialog.dismiss()

    def updateProgress(self, fileId, totalFiles, *args):
        if self.dialog:
            self.dialog.content_cls.fileId = fileId
            self.dialog.content_cls.totalFiles = totalFiles
            if fileId == totalFiles:
                self.dismiss()