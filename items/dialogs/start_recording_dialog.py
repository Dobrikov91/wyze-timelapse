from tkinter.filedialog import askdirectory

from kivy.app import App
from kivy.lang import Builder
from kivy.properties import StringProperty
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.button import MDRectangleFlatButton
from kivymd.uix.dialog import MDDialog

from items.buttons import SimpleToggleButton

Builder.load_string('''
<StartRecordingView>:
    orientation: 'horizontal'
    size_hint_y: None
    height: dp(36)
    spacing: dp(8)

    folderPath: app.config.get('recording', 'folderToSave')

    TextInput:
        id: folderPathText
        size_hint_x: 3

        readonly: True
        text: root.folderPath

    MDRectangleFlatButton:
        text: 'Choose folder'
        size_hint_x: 1
        on_release: root.chooseFolder()
''')

class StartRecordingView(MDBoxLayout):
    folderPath = StringProperty('')

    def chooseFolder(self):
        self.folderPath = askdirectory(
            initialdir=App.get_running_app().config.get('recording', 'folderToSave'))
        if self.folderPath:
            App.get_running_app().config.set('recording', 'folderToSave', self.folderPath)

class StartRecordingDialog:
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.dialog = None
        self.onSelectedCallback = lambda: App.get_running_app().controller.startRecording()

    def show(self):
        if not self.dialog:
            self.dialog = MDDialog(
                title='Set recording params',
                type='custom',
                content_cls=StartRecordingView(),
                buttons=[
                    MDRectangleFlatButton(
                        text='CANCEL',
                        on_release=self.close),
                    MDRectangleFlatButton(
                        text='OK',
                        on_release=self.selected),
                ],
            )
        self.dialog.open()

    def close(self, *args):
        App.get_running_app().controller.view.ids['overlappingView'].ids['recordButton'].state = 'normal'
        self.dialog.dismiss()

    def selected(self, *args):
        self.dialog.dismiss()
        if self.onSelectedCallback:
            self.onSelectedCallback()