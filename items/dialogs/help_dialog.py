from kivy.lang import Builder
from kivymd.uix.button import MDRectangleFlatButton
from kivymd.uix.dialog import MDDialog
from kivymd.uix.gridlayout import MDGridLayout
from kivymd.uix.label import MDLabel
from kivymd.uix.textfield import MDTextField

help_text = '''
1. Select folder with videofiles recorded by Wyze Cam
File names should have paths like this:
[b][color=1DE9B6].../YYYYMMDD/HH/MM.mp4[/color][/b] (default Wyze format)
otherwise they will be ignored

2. Hotkey navigation:
- [b]Left/Right[/b] arrows: jump backward/forward 2% of video duration
- [b]Space[/b]: play/pause

3. Choose right video speed with slider on top
Accelerated length will show real video length

4. Record video with [b]Record[/b] button
Recordings always start from the beginning of the video till the end

Preview could be laggy on slow devices, recorded video will be smooth

New features are coming soon, dont forget to check for updates with down arrow button

For any questions or suggestions please contact me (info in About button)
'''

class HelpDialog:
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.dialog = None

    def show(self):
        if not self.dialog:
            self.dialog = MDDialog(
                title='Help',
                text=help_text,
                buttons=[
                    MDRectangleFlatButton(
                        text='CLOSE',
                        on_release=lambda x: self.close()
                    )
                ],
            )
        self.dialog.open()

    def close(self, *args):
        if self.dialog:
            self.dialog.dismiss()