from kivy.app import App
from kivy.lang import Builder
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.button import MDFlatButton, MDRectangleFlatButton
from kivymd.uix.dialog import MDDialog
from kivymd.uix.label import MDLabel
from kivymd.uix.spinner import MDSpinner

Builder.load_string('''
<StreamConnectingView>:
    size_hint: None, None
    size: dp(36*10), dp(36)

    MDLabel:
        text: 'Stream connecting'
        haling: 'center'

        MDSpinner:
            size_hint: None, None
            size: dp(36), dp(36)
            center: self.parent.center
            active: True
''')

class StreamConnectingView(MDBoxLayout):
    pass

class StreamConnectingDialog(MDDialog):
    def __init__(self, **kwargs):
        super().__init__(
            type='custom',
            content_cls=StreamConnectingView(),
            auto_dismiss=False,
            buttons=[
                MDFlatButton(text='Cancel', on_release=self.cancel),
            ]
        )

    def cancel(self, *args):
        self.dismiss()
        App.get_running_app().controller.stopSource()


if __name__ == "__main__":
    from kivymd.app import MDApp

    class Test(MDApp):
        def build(self):
            self.theme_cls.theme_style = "Dark"
            self.dialog = StreamConnectingDialog()
            self.dialog.open()
            return MDBoxLayout()

    Test().run()