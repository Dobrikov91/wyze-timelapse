import platform
import webbrowser

import requests
from kivy.app import App
from kivymd.uix.button import MDRectangleFlatButton
from kivymd.uix.dialog import MDDialog


class UpdateDialog():
    def __init__(self):
        self.dialog = None
        self.releasePage = 'https://gitlab.com/api/v4/projects/Dobrikov91%2Fwyze-timelapse/releases'
        self.downloadUrl = ''
        self.sysplatform = platform.system()
        self.arch = platform.machine()

    def show(self):
        self.dialog = MDDialog(
            title='Checking for updates...',
            text='Please wait...',
            buttons=[
                MDRectangleFlatButton(
                    text='Close',
                    on_release=lambda _: self.hide()
                ),
                MDRectangleFlatButton(
                    text='Open download link',
                    disabled=True,
                    on_release=lambda _: webbrowser.open(self.downloadUrl)
                ),
            ]
        )
        self.dialog.open()
        self.checkForNewRelease()

    def hide(self):
        self.dialog.dismiss()

    # names from gitlab
    def get_os_name(self) -> str:
        if self.sysplatform == 'Linux':
            return 'Ubuntu x64'
        elif self.sysplatform == 'Windows':
            return 'Windows x64'
        elif self.sysplatform == 'Darwin':
            if self.arch == 'arm64':
                return 'MacOS Arm'
            elif self.arch == 'x86_64':
                return 'MacOS x64'
            else:
                return ''
        else:
            return ''

    # get latest release gitlab
    def checkForNewRelease(self) -> None:
        try:
            response = requests.get(self.releasePage, timeout=5)
            if response.status_code == 200:
                for link in response.json()[0]['assets']['links']:
                    if link['name'] == self.get_os_name():
                        newVersion = response.json()[0]['tag_name'][1:] # remove 'v' from version
                        self.downloadUrl = link['url']

                        if newVersion > App.get_running_app().version:
                            self.dialog.text = f'New version {newVersion} is available. Do you want to download it?'
                            self.dialog.buttons[0].text = 'Later'
                            #self.dialog.buttons[1].text = 'Open download link'
                            self.dialog.buttons[1].disabled = False
                            return
                        else:
                            self.dialog.text = 'You have the latest version'
                            return
            self.dialog.text = 'Can\'t check for updates'
        except:
            self.dialog.text = 'Error while checking for updates'