from kivy.app import App
from kivy.lang import Builder
from kivymd.uix.slider import MDSlider

Builder.load_string('''
<SliderBase>:
    name: ''
    section: ''
    suffix: ''
    customValue: self.value
    valueText: '{:.2f} {}'.format(self.customValue, self.suffix)
    max: 1000 # workaround for sliders with max greater than 100

    hint: False
    size_hint_y: None
    height: dp(36)
    show_off: False

    MDRelativeLayout:
        #md_bg_color: 0, 1, 0, 0.5
        size: self.parent.size
        pos: self.parent.pos

        MDBoxLayout:
            #md_bg_color: 0, 1, 0, 0.5
            orientation: 'horizontal'
            padding: dp(8), dp(0)

            MDLabel:
                #md_bg_color: 1, 0, 0, 0.5
                adaptive_size: True
                pos_hint: {'top': 1.1, 'left': 1}
                text: root.name

            MDLabel:
                #md_bg_color: 0, 0, 1, 0.5
                adaptive_height: True
                halign: 'right'
                pos_hint: {'top': 1.1, 'right': 1}
                text: root.valueText

<ConfigSliderFloat>:
    value: float(self.readValue(self.section, self.name))
    on_touch_up: self.writeValue(*args[1].pos, float(self.value))
    on_touch_move: self.writeValue(*args[1].pos, float(self.value))

<ConfigSliderInt>:
    value: self.readValue(self.section, self.name)
    customValue: int(self.value)
    on_touch_up: self.writeValue(*args[1].pos, int(self.customValue))
    on_touch_move: self.writeValue(*args[1].pos, int(self.customValue))
    valueText: '{} {}'.format(int(self.customValue), self.suffix)

<ConfigSliderMap>:
    values: [1]
    min: 0
    max: len(self.values) - 1
    step: 1
    indexValue: int(self.readValue(self.section, self.name))
    value: self.values.index(self.indexValue) if self.indexValue in self.values else 0
    customValue: self.values[int(self.value)]
    on_touch_up: self.writeValue(*args[1].pos, int(self.customValue))
    on_touch_move: self.writeValue(*args[1].pos, int(self.customValue))

<ConfigSliderFPS>:
    values: [x * y for y in [1, 30, 900] for x in [1, 2, 3, 4, 5, 10, 15, 20] ]
    suffix: 'x'
    valueText: '{} {}'.format(int(self.customValue), self.suffix)
''')

class SliderBase(MDSlider):
    def readValue(self, section, name):
        try:
            return App.get_running_app().config.get(section, name)
        except:
            return 0

    def writeValue(self, x, y, value):
        if (self.collide_point(x, y) and
            not self.disabled):
            App.get_running_app().config.set(self.section, self.name, value)

class ConfigSliderFloat(SliderBase):
    pass

class ConfigSliderInt(SliderBase):
    pass

class ConfigSliderMap(SliderBase):
    pass

class ConfigSliderFPS(ConfigSliderMap):
    pass

if __name__ == '__main__':
    from kivymd.app import MDApp

    class Test(MDApp):
        def build(self):
            self.theme_cls.theme_style = "Dark"
            return ConfigSliderFloat()

    Test().run()