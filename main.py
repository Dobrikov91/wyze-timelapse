import cProfile
import os
# NSException when creating tkroot after kivy
from tkinter import Tk

root = Tk()
root.withdraw()

from kivy.config import Config
from kivy.core.window import Window
from kivy.utils import platform
from kivymd.app import MDApp
from kivymd.icon_definitions import md_icons  # need for pyinstaller

from controller import Controller
from tools.config import defaultConfig
from tools.utils import resource_path

if platform != 'macosx':
    from charset_normalizer import md__mypyc  # need for Win and Ubuntu

Config.set('input', 'mouse', 'mouse,multitouch_on_demand')

class WyzeTimelapse(MDApp):
    icon = resource_path('timelapse.png')

    def build_config(self, config):
        for section in defaultConfig:
            config.setdefaults(section, defaultConfig[section])

    def get_application_config(self):
        return str(os.path.join(self.user_data_dir, 'wyze_timelapse.ini'))

    def open_settings(self, *largs):
        # block F1 settings
        pass

    def build(self):
        self.version = '1.0.1'
        self.theme_cls.material_style = 'M3'
        self.theme_cls.theme_style = 'Dark'
        self.theme_cls.primary_palette = 'Teal'
        self.theme_cls.primary_hue = 'A400'
        self.controller = Controller(self.config)
        Window.size = (800, 593)
        Window.bind(on_keyboard=self.on_keyboard)
        self.title = 'Wyze timelapse {}'.format(self.version)
        return self.controller.view

    def on_keyboard(self, window, key, scancode, codepoint, modifier):
        if key == 32: # space
            self.controller.playPause()
        elif key == 276: # left
            self.controller.backward()
        elif key == 275: # right
            self.controller.forward()

    def on_start(self):
        self.profile = None # cProfile.Profile()
        if self.profile:
            self.profile.enable()

    def on_stop(self):
        if self.profile:
            self.profile.disable()
            self.profile.dump_stats('myapp.profile')

        self.controller.stop()
        self.config.write()

if __name__ == '__main__':
    WyzeTimelapse().run()
