import pstats
p = pstats.Stats('../myapp.profile')
# skip strip_dirs() if you want to see full path's

p.strip_dirs().sort_stats('calls').print_stats(10)
p.strip_dirs().sort_stats('time').print_stats(10)


#p.strip_dirs().print_stats()

'''
calls (call count)
cumulative (cumulative time)
cumtime (cumulative time)
file (file name)
filename (file name)
module (file name)
ncalls (call count)
pcalls (primitive call count)
line (line number)
name (function name)
nfl (name/file/line)
stdname (standard name)
time (internal time)
tottime (internal time)
'''