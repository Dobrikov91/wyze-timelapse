from pathlib import Path

defaultConfig = {
    'controls': {
        'inputFolder': str(Path.home()),
        'printTime': False,
        'acceleration': 5,
    },
    'recording': {
        'folderToSave': str(Path.home()),
        'filePrefix': 'WyzeTimelapse'
    },
}
