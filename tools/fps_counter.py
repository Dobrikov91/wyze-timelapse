import queue
import statistics
from datetime import datetime
from time import sleep
from typing import Optional


class FpsCounter:
    def __init__(self, queueSize: int) -> None:
        self.queueSize = queueSize
        self.fpsValues = queue.Queue()
        self.prevFrameTime: Optional[datetime] = None
        self.fps: float = 0
        self.averageFps: float = 0
        self.frameId: int = 0

    def update(self) -> None:
        self.frameId += 1

        now = datetime.now()
        if self.prevFrameTime is not None:
            timeDiff = (now - self.prevFrameTime).microseconds + 1000000 * (now - self.prevFrameTime).seconds

            if timeDiff > 0:
                self.fps = 1000000 / timeDiff
            else:
                self.fps = 0

            self.fpsValues.put(self.fps)
            while self.fpsValues.qsize() > self.queueSize:
                self.fpsValues.get()
            self.averageFps = statistics.mean(list(self.fpsValues.queue))
        self.prevFrameTime = now

    def reset(self) -> None:
        self.fpsValues.queue.clear()
        self.prevFrameTime = datetime.now()


if __name__ == '__main__':
    # test 30fps
    fpsCounter = FpsCounter(1000)
    for i in range(1, 1000):
        fpsCounter.update()
        if i % 10 == 0:
            print(fpsCounter.averageFps)
        sleep(1.0 / 30)
