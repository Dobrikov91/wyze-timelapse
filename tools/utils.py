import os
import sys
from datetime import datetime

import cv2 as cv


def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath('.')

    return os.path.join(base_path, relative_path)

def getStrDateTime(time):
    strTime = str(time)
    if '.' not in strTime:
        return strTime
    return strTime[:strTime.find('.')]

def printTimeOnImage(image: cv.Mat, time: datetime):
    scale = image.shape[0] / 720
    position = (int(image.shape[1] - 380 * scale), int(image.shape[0] - 20 * scale))
    font = cv.FONT_HERSHEY_SIMPLEX
    return cv.putText(image, getStrDateTime(time), position, font, scale, (0, 255, 0), int(2 * scale), cv.LINE_AA)
