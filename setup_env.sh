deactivate
rm -rf .venv

python3.11 -m venv .venv
source .venv/bin/activate

python -m pip install kivy --pre --no-deps --index-url  https://kivy.org/downloads/simple/
python -m pip install "kivy[base]" --pre --extra-index-url https://kivy.org/downloads/simple/

pip install -r requirements.txt

name="WyzeTimelapse"
version="1.0.1"
output_file="./${name}_${version}.dmg"

pyinstaller -y --clean ./builds/WyzeTimelapseMac.spec

pushd dist
hdiutil create $output_file -srcfolder WyzeTimelapse.app -ov
popd