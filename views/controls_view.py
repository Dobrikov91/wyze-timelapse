from datetime import datetime

from kivy.lang import Builder
from kivy.properties import NumericProperty, ObjectProperty
from kivymd.uix.boxlayout import MDBoxLayout

from items.labels import FrameStepLabel, TimeLabel
from items.sliders import ConfigSliderFPS

Builder.load_string('''
<ControlsView>:
    ConfigToggleButton:
        id: printTime
        section: 'controls'
        name: 'printTime'
        text: 'Print time'

    MDLabel:
        text: 'Frame step'
        halign: 'center'
        size_hint_x: None
        width: dp(50)

    FrameStepLabel:
        id: frameStepLabel
        step: root.frameStep

    ConfigSliderFPS:
        id: acceleration
        section: 'controls'
        name: 'acceleration'
        size_hint_x: 3

    TimeLabel:
        id: acceleratedLengthLabel
        time: root.acceleratedLength

    MDLabel:
        text: 'Accelerated length'
        halign: 'center'
        size_hint_x: None
        width: dp(90)
''')

class ControlsView(MDBoxLayout):
    frameStep = NumericProperty(0)
    acceleratedLength = ObjectProperty(datetime(1900, 1, 1))