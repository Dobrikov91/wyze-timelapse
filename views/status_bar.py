from kivy.event import EventDispatcher
from kivy.lang import Builder
from kivy.properties import NumericProperty, StringProperty
from kivymd.uix.gridlayout import MDGridLayout

Builder.load_string('''
<StatusBar>:
    rows: 1
    cols: 3

    MDLabel:
        halign: 'center'
        text: 'Stream: {}x{:.0f}'.format(root.streamResolution, root.streamFps)

    MDLabel:
        halign: 'center'
        text: 'FPS: {:.2f}'.format(root.inputFps)

    MDLabel:
        halign: 'center'
        text: 'Frame: {}'.format(root.totalFrames)
''')

class StatusBar(MDGridLayout, EventDispatcher):
    streamResolution = StringProperty(str((0,0)))
    streamFps = NumericProperty(0)
    inputFps = NumericProperty(0)
    totalFrames = NumericProperty(0)

    def update(self, streamResolution, streamFps, inputFps, totalFrames):
        self.streamResolution = str(streamResolution)
        self.streamFps = streamFps
        self.inputFps = inputFps
        self.totalFrames = totalFrames

    def reset(self):
        self.update((0,0), 0, 0, 0)
