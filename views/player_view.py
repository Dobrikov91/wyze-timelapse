from datetime import datetime

import cv2 as cv
import numpy as np
from kivy.app import App
from kivy.clock import Clock
from kivy.graphics.texture import Texture
from kivy.lang import Builder
from kivy.properties import ObjectProperty
from kivy.uix.image import Image
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.button import MDIconButton
from kivymd.uix.slider import MDSlider

from items.labels import TimeLabel

Builder.load_string('''
<PlayerView>:
    id: playerView
    orientation: 'vertical'
    disabled: True

    Image:
        id: image

    MDBoxLayout:
        id: playbackControls
        orientation: 'horizontal'
        spacing: dp(6)
        size_hint_y: None
        height: dp(36)

        PlayPauseButton:
            id: playPauseButton
            icon: 'play'
            md_bg_color: app.theme_cls.primary_color
            icon_size: dp(12)

        TimeLabel:
            time: root.currentLength

        MDSlider:
            id: slider
            min: 0
            max: 1
            hint: False
            show_off: False
            on_touch_down: if self.collide_point(*args[1].pos): root.startSeeking()
            on_touch_move: if self.collide_point(*args[1].pos): root.startSeeking()
            on_touch_up: root.on_seek()

        TimeLabel:
            time: root.length
''')

class PlayPauseButton(MDIconButton):
    def __init__(self, **kwargs):
        self.icon = 'play'
        super().__init__(**kwargs)

    def on_press(self):
        App.get_running_app().controller.playPause()

class PlayerView(MDBoxLayout):
    currentLength = ObjectProperty(datetime(1900, 1, 1))
    length = ObjectProperty(datetime(1900, 1, 1))

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        Clock.schedule_once(self.clear, 0)

    def on_frame(self, frame):
        if frame is None:
            return

        if len(frame.shape) == 2: # grayscale
            frame = cv.cvtColor(frame, cv.COLOR_GRAY2RGB)
        else:
            frame = cv.cvtColor(frame, cv.COLOR_BGR2RGB)

        self.ids['image'].texture = Texture.create(size=(frame.shape[1], frame.shape[0]), colorfmt='rgb') # android don't support bgr
        self.ids['image'].texture.blit_buffer(frame.tobytes(), colorfmt='rgb', bufferfmt='ubyte')
        self.ids['image'].texture.flip_vertical()
        self.update()

    def on_play(self):
        self.ids['playPauseButton'].icon = 'pause'

    def on_pause(self):
        self.ids['playPauseButton'].icon = 'play'

    def on_stop(self):
        pass

    def isPlaying(self):
        return self.ids['playPauseButton'].icon == 'pause'

    def startSeeking(self):
        self.isSeeking = True
        App.get_running_app().controller.seek(self.ids['slider'].value)

    def on_seek(self):
        App.get_running_app().controller.seek(self.ids['slider'].value)
        self.isSeeking = False

    def update(self):
        if not App.get_running_app().controller.frameProvider:
            return

        if not self.isSeeking:
            self.ids['slider'].value = App.get_running_app().controller.frameProvider.position

        try:
            self.currentLength = datetime(1900, 1, 1) + App.get_running_app().controller.frameProvider.currentLength
            self.length = datetime(1900, 1, 1) + App.get_running_app().controller.frameProvider.length
        except:
            pass

    def clear(self, *args):
        self.currentLength = datetime(1900, 1, 1)
        self.length = datetime(1900, 1, 1)
        self.isSeeking = False
        self.on_frame(np.full((1080, 1920), 100, dtype=np.uint8))
