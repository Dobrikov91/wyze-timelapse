from kivy.lang import Builder
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.floatlayout import MDFloatLayout

from items.buttons import SimpleToggleButton
from views.controls_view import ControlsView
from views.player_view import PlayerView

Builder.load_string('''
<OverlappingView>:
    #md_bg_color: 0.5, 0.5, 0.5, 1

    PlayerView:
        id: playerView
        pos_hint: {'center_x': .5, 'center_y': .5}

    ControlsView:
        id: controlsView
        orientation: 'horizontal'
        pos_hint: {'top': 1, 'center_x': .5}
        md_bg_color: 0, 0, 0, 0.5
        size_hint: 1, None
        height: dp(36)
        spacing: dp(6)

    MDBoxLayout:
        size_hint: None, None
        height: self.minimum_height
        width: self.minimum_width
        pos_hint: {'y': .1, 'center_x': .5}
        md_bg_color: 0, 0, 0, 0.5

        SimpleToggleButton:
            id: recordButton
            text: 'RECORD'
            disabled: True
            on_press: app.controller.startRecordingDialog.show() if self.state == 'down' else app.controller.stopRecording()
''')

class OverlappingView(MDFloatLayout):
    pass