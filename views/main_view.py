from kivy.app import App
from kivy.lang import Builder
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.toolbar import MDTopAppBar

from views.overlapping_view import OverlappingView
from views.status_bar import StatusBar

Builder.load_string('''
<View>:
    orientation: 'vertical'

    TopBar:
        id: topBar

    MDBoxLayout:
        orientation: 'vertical'
        padding: dp(8)
        spacing: dp(8)

        OverlappingView:
            id: overlappingView

    StatusBar:
        id: statusBar
        size_hint_y: None
        height: dp(36)
        disabled: True
''')

class TopBar(MDTopAppBar):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.title = '<= Select Wyze Folder'
        self.left_action_items = [
            [
                'folder-play',
                lambda _: App.get_running_app().controller.selectFolder(),
                'Select folder',
            ],
        ]
        self.right_action_items = [
            [
                'help-circle',
                lambda _: App.get_running_app().controller.helpDialog.show(),
                'Help',
            ],
            [
                'download-circle',
                lambda _: App.get_running_app().controller.updateDialog.show(),
                'Check for updates'
            ],
            [
                'information',
                lambda _: App.get_running_app().controller.showAbout(),
                'About',
            ]
        ]


class View(MDBoxLayout):
    pass
