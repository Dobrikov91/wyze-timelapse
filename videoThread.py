from datetime import datetime
from threading import Event, Thread
from typing import Optional

from input.file_list import FileList
from input.recorder import Recorder
from tools.fps_counter import FpsCounter
from tools.utils import printTimeOnImage


class InputVideoThread():
    def __init__(self,
            frameProvider: Optional[FileList],
            acceleration: int,
            printTime: bool):
        Thread.__init__(self)

        self.thread: Optional[Thread] = None
        self.stopped = Event()
        self.stopped.set()

        self.threadStopped = Event()
        self.threadStopped.set()

        self.frameProvider = frameProvider
        self.acceleration = acceleration
        self.printTime = printTime
        self.recorder: Optional[Recorder] = None
        self.fpsCounter = FpsCounter(10)

        self.prevTime = datetime.now()
        self.currentFrame = None

    def playingThread(self) -> None:
        prevTime = datetime.now()
        while not self.stopped.wait(
                self.frameProvider.frameInterval -
                (datetime.now() - prevTime).seconds -
                (datetime.now() - prevTime).microseconds / 1000 / 1000 -
                9e-2 * self.frameProvider.frameInterval):
            prevTime = datetime.now()
            self.showFrame()
            self.jumpToNextFrame()
        self.threadStopped.set()
        print('Thread stopped')

    def recordThread(self) -> None:
        while True:
            if self.stopped.wait(0):
                break
            self.showFrame()
            self.jumpToNextFrame()
        self.threadStopped.set()
        print('Recording thread stopped')

    def isPlaying(self) -> bool:
        return not self.stopped.is_set()

    # start playing = create thread
    def play(self):
        if self.isPlaying():
            print('Already playing')
            return

        self.stopped.clear()
        self.threadStopped.clear()

        self.thread = Thread(target=self.playingThread)
        self.thread.start()
        print('Playing')

    def record(self):
        if self.isPlaying():
            self.stop()

        self.stopped.clear()
        self.threadStopped.clear()

        self.thread = Thread(target=self.recordThread)
        self.thread.start()
        print('Recording')

    def setStopped(self):
        self.stopped.set()

    # stop playing = stop thread (should be called only outside of thread)
    def pause(self):
        if not self.isPlaying():
            print('Already stopped')
            return

        self.setStopped()
        print('Stopping')
        self.threadStopped.wait()
        print('Stopped')

    def stop(self):
        self.pause()
        self.seek(0)

    def showFrame(self):
        frame = self.frameProvider.getFrame()
        if frame is None:
            print('No frame')
            return

        if self.printTime:
            printTimeOnImage(frame, self.frameProvider.frameDateTime)

        self.currentFrame = frame
        self.fpsCounter.update()

        if self.recorder:
            self.recorder.writeFrame(self.currentFrame)

    def jumpToNextFrame(self):
        if self.acceleration > 1:
            if self.acceleration < 10:
                for _ in range(self.acceleration - 1):
                    self.frameProvider.getFrame()
            else:
                newPosition = (self.frameProvider.position +
                    (self.acceleration - 1) / self.frameProvider.totalFrames)
                if newPosition >= 1.0:
                    print('jumpToNextFrame: end of video')
                    self.setStopped()
                    if self.frameProvider.videoEndedCallback:
                        self.frameProvider.videoEndedCallback()
                    return

                self.frameProvider.seek(newPosition)

    def seek(self, position):
        wasPlaying = self.isPlaying()
        if wasPlaying:
            self.pause()

        if self.frameProvider:
            self.frameProvider.seek(position)
            self.showFrame()

        if wasPlaying:
            self.play()