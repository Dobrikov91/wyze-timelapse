brew install pkg-config sdl2 sdl2_image sdl2_ttf sdl2_mixer
python3.11 --version

python3.11 -m pip install kivy --pre --no-deps --index-url  https://kivy.org/downloads/simple/
python3.11 -m pip install "kivy[base]" --pre --extra-index-url https://kivy.org/downloads/simple/
python3.11 -m pip install -r requirements.txt

name="WyzeTimelapse"
version="1.0.1"
output_file="./${name}_${version}.dmg"

pyinstaller -y --clean ./builds/WyzeTimelapseMac.spec

pushd dist
hdiutil create $output_file -srcfolder WyzeTimelapse.app -ov
popd