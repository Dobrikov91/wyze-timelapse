from typing import Optional

import cv2 as cv

from input.video_properties import VideoProperties


class File(VideoProperties):
    def __init__(self, path: str) -> None:
        super().__init__()
        self.path = path
        self.source = None
        self.loadProperties()
        self.relativePosition = 0.0

    def open(self) -> None:
        if not self.source:
            self.source = cv.VideoCapture(self.path)

    def loadProperties(self):
        if not self.source:
            self.source = cv.VideoCapture(self.path)

        self.resolution = (int(self.source.get(cv.CAP_PROP_FRAME_WIDTH)),
                            int(self.source.get(cv.CAP_PROP_FRAME_HEIGHT)))
        self.fps = self.source.get(cv.CAP_PROP_FPS)

        self.position = 0.0
        self.totalFrames = self.source.get(cv.CAP_PROP_FRAME_COUNT)
        self.startDateTime = self.getTimestampFromPath(self.path)

        # release source after to keep memory free
        self.source.release()
        self.source = None

        self.updateProperties()

    def close(self) -> None:
        if self.source:
            self.source.release()
            self.source = None

    def seek(self, position: float) -> None:
        self.open()
        self.source.set(cv.CAP_PROP_POS_FRAMES, position * self.totalFrames)
        self.position = position
        self.updateProperties()

    def getFrame(self) -> Optional[cv.Mat]:
        self.open()

        if not self.source.isOpened():
            return None

        ret, frame = self.source.read()
        if not ret:
            return None

        self.position += 1 / self.totalFrames
        self.updateProperties()
        return frame