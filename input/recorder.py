import os
from datetime import datetime, timedelta
from pathlib import Path
from typing import Optional

import cv2 as cv

from tools.fps_counter import FpsCounter
from tools.utils import getStrDateTime


class Recorder:
    def __init__(self,
            resolution: tuple,
            fps: int,
            folderToSave: str,
            filePrefix: str) -> None:

        self.resolution = resolution
        self.fps = fps

        self.folderToSave = os.path.abspath(folderToSave)
        self.filePrefix = filePrefix

        self.length = timedelta(seconds=0)
        self.startTime: Optional[datetime] = None

        self.fpsCounter = FpsCounter(10)
        self.path: Optional[str] = None
        self.file = None

    def __del__(self) -> None:
        self.closeFile()

    def createFile(self) -> None:
        Path(self.folderToSave).mkdir(parents=True, exist_ok=True)
        print('Recorder path {}'.format(self.folderToSave))

        self.path = '{}/{}_{}.mp4'.format(
            self.folderToSave,
            self.filePrefix,
            getStrDateTime(datetime.now()).replace(':','_'))

        self.file = cv.VideoWriter(self.path,
                        cv.VideoWriter_fourcc(*'mp4v'),
                        self.fps,
                        self.resolution)

        self.startTime = datetime.now()
        print('VideoFile {} created'.format(self.path))
        print('Resolution {}, fps {}'.format(
            self.resolution,
            self.fps))

    def closeFile(self) -> None:
        if self.file:
            self.file.release()
            print('Release file')
        self.file = None
        print('VideoFile {} closed'.format(self.path))

    def writeFrame(self, frame):
        self.fpsCounter.update()

        if self.file is None:
            self.createFile()

        if frame.shape[0] != self.resolution[1] or frame.shape[1] != self.resolution[0]:
            frame = cv.resize(frame, self.resolution)

        if self.file:
            self.file.write(frame)
        self.length += timedelta(seconds=1.0 / self.fps)