from abc import ABC
from datetime import datetime, timedelta
from pathlib import Path

# 0               position      | 1              : float
# 0               currentFrame  | totalFrames    : int
# 0               currentLength | length         : timedelta
# startDateTime | frameDateTime | endDateTime    : datetime

class VideoProperties(ABC):
    def __init__(self) -> None:
        self.path: str = ''
        self.source = None
        self.resolution: tuple = (0, 0)
        self.fps: float = 1

        self.position: float = 0.0
        self.totalFrames: int = 0
        self.startDateTime = datetime.now()

        # calculated
        self.frameInterval = 1.0
        self.currentFrame: int = 0
        self.currentLength: timedelta(seconds=0.0)
        self.length: float = timedelta(seconds=0.0)

        self.frameDateTime = datetime.now()
        self.endDateTime = datetime.now()

    def updateProperties(self) -> None:
        self.frameInterval = 1 / self.fps

        self.currentFrame = int(self.position * self.totalFrames)
        self.length = timedelta(seconds=self.totalFrames * self.frameInterval)
        self.currentLength = self.length * self.position

        self.frameDateTime = self.startDateTime + self.currentLength
        self.endDateTime = self.startDateTime + self.length

    def getTimestampFromPath(self, path: str) -> datetime:
        pathItems = Path(path).parts
        minute = int(Path(pathItems[-1]).stem)
        hour = int(pathItems[-2])

        year = int(pathItems[-3][:4])
        month = int(pathItems[-3][4:6])
        day = int(pathItems[-3][6:8])
        return datetime(year=year, month=month, day=day, hour=hour, minute=minute, second=0)