import glob
from typing import Optional

import cv2 as cv

from input.file import File
from input.video_properties import VideoProperties
from tools.fps_counter import FpsCounter


class FileList(VideoProperties):
    def __init__(self,
            path: str,
            fileLoadedCallback = None,
            createdCallback = None) -> None:
        super().__init__()

        self.path = path
        self.fileLoadedCallback = fileLoadedCallback
        self.createdCallback = createdCallback
        self.videoEndedCallback = None

        self.loadingFiles = True

        self.inputFiles: list[File] = []
        self.currentFile: Optional[File] = None
        self.fpsCounter = FpsCounter(10)

    def getFileList(self) -> list[str]:
        # .mp4
        patterns = ['/**/*.[mM][pP]4',]
        file_list = []

        for pattern in patterns:
            file_list += glob.glob(self.path + pattern, recursive=True)
        file_list.sort()
        return file_list

    def loadFiles(self) -> None:
        file_list = self.getFileList()
        for file_path in file_list:
            if not self.loadingFiles:
                return
            try:
                inputFile = File(file_path)
                self.inputFiles.append(inputFile)
                self.appendMetadataFromFile(inputFile)
                if self.fileLoadedCallback:
                    self.fileLoadedCallback(len(self.inputFiles), len(file_list))
            except:
                print('File error:', file_path)

        if len(self.inputFiles) == 0:
            return

        self.updateFilesRelativePosition()
        self.currentFile = self.inputFiles[0]
        self.startDateTime = self.currentFile.startDateTime
        self.updateProperties()

        if self.createdCallback:
            self.createdCallback()

    def closeFiles(self) -> None:
        for file in self.inputFiles:
            file.close()

    def appendMetadataFromFile(self, file: File) -> None:
        self.resolution = file.resolution
        self.fps = max(self.fps, file.fps)
        self.position = 0.0
        self.totalFrames += file.totalFrames

    def updateFilesRelativePosition(self) -> None:
        tempPosition = 0
        for file in self.inputFiles:
            file.relativePosition = tempPosition
            tempPosition += file.totalFrames / self.totalFrames

    def seek(self, position: float) -> None:
        self.position = position
        self.updateProperties()

        for file in self.inputFiles:
            fileLeft = file.relativePosition
            fileRight = file.relativePosition + file.totalFrames / self.totalFrames

            if fileLeft <= position and position < fileRight:
                restPosition = position - fileLeft
                positionInFile = restPosition * self.totalFrames / file.totalFrames

                # dont reconnect if we are already in the right file
                if self.currentFile == file:
                    self.currentFile.seek(positionInFile)
                    break

                self.currentFile.close()
                self.currentFile = file
                self.currentFile.open()
                self.currentFile.seek(positionInFile)
                break

    def endOfInput(self) -> None:
        self.seek(0)
        self.frameDateTime = self.currentFile.frameDateTime
        if self.videoEndedCallback:
            self.videoEndedCallback()

    def getFrame(self) -> Optional[cv.Mat]:
        frame = self.currentFile.getFrame()
        if frame is not None:
            self.position += 1 / self.totalFrames
            self.fpsCounter.update()
            self.updateProperties()
            return frame

        if self.inputFiles.index(self.currentFile) == len(self.inputFiles) - 1:
            self.endOfInput()
            return None

        # try to jump to next file
        self.currentFile.close()
        self.currentFile = self.inputFiles[
            self.inputFiles.index(self.currentFile) + 1]
        self.currentFile.open()

        frame = self.currentFile.getFrame()
        if frame is None:
            self.endOfInput()
            return None
        self.position += 1 / self.totalFrames
        self.fpsCounter.update()
        self.updateProperties()
        return frame