#pyinstaller -y --clean --windowed --name WyzeTimelapse \
#  --exclude-module enchant \
#  --exclude-module twisted \
#  ../main.py
# bumpversion --config-file .bumpversion.cfg patch
# remove --windowed to --onefile to see an issue

name="WyzeTimelapse"
version="1.0.1"
output_file="./${name}_${version}.dmg"

pyinstaller -y --clean ./WyzeTimelapseMac.spec

pushd dist
hdiutil create $output_file -srcfolder WyzeTimelapse.app -ov
popd