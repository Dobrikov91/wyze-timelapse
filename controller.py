from datetime import datetime, timedelta
from enum import Enum
from threading import Thread
from tkinter.filedialog import askdirectory
from typing import Optional

from kivy.app import App
from kivy.clock import Clock
from kivy.config import ConfigParser

from input.file_list import FileList
from input.recorder import Recorder
from items.dialogs.about_dialog import AboutDialog
from items.dialogs.help_dialog import HelpDialog
from items.dialogs.loading_files_dialog import LoadingFilesDialog
from items.dialogs.recorded_dialog import RecordedDialog
from items.dialogs.start_recording_dialog import StartRecordingDialog
from items.dialogs.stream_connecting_dialog import StreamConnectingDialog
from items.dialogs.update_dialog import UpdateDialog
from tools.fps_counter import FpsCounter
from videoThread import InputVideoThread
from views.main_view import View


class Factory:
    def __init__(self, config: ConfigParser) -> None:
        self.config = config

    def getRecorder(self, resolution: tuple, fps: int) -> Recorder:
        section = 'recording'

        folderToSave = self.config.get(section, 'folderToSave')
        filePrefix = self.config.get(section, 'filePrefix')
        return Recorder(
            resolution,
            fps,
            folderToSave,
            filePrefix)

class State(Enum):
    Stopped = 0
    Playing = 1
    Recording = 2

class Controller:
    playingEvent = None

    def __init__(self, config: ConfigParser) -> None:
        self.config = config
        self.config.add_callback(self.setParam)

        self.state = State.Stopped
        self.factory = Factory(self.config)
        self.frameProvider: Optional[FileList] = None
        self.recorder: Optional[Recorder] = None
        self.view = View()
        self.fpsCounter = FpsCounter(10)

        self.printTime = self.config.get('controls', 'printTime') == 'True'
        self.acceleration = int(self.config.get('controls', 'acceleration'))

        self.inputVideoThread = InputVideoThread(
            self.frameProvider,
            self.acceleration,
            self.printTime)

        self.updateViewEvent = None

        self.loadingFilesThread = None
        self.loadingFilesDialog = LoadingFilesDialog()
        self.connectingDialog = StreamConnectingDialog()
        self.startRecordingDialog = StartRecordingDialog()
        self.recordedDialog = RecordedDialog()
        self.helpDialog = HelpDialog()
        self.updateDialog = UpdateDialog()
        self.aboutDialog = AboutDialog()

    def setParam(self, section, key, value):
        if section == 'controls':
            if key == 'acceleration':
                if not self.frameProvider:
                    return
                self.acceleration = int(value)
                self.inputVideoThread.acceleration = self.acceleration
                self.view.ids['overlappingView'].ids['controlsView'].acceleratedLength = datetime(1900, 1, 1) + self.frameProvider.length / self.acceleration
                self.view.ids['overlappingView'].ids['controlsView'].frameStep = self.frameProvider.frameInterval * self.acceleration

            elif key == 'printTime':
                self.printTime = value
                self.inputVideoThread.printTime = self.printTime

    def updateStatusBar(self):
        self.view.ids['statusBar'].update(
            self.frameProvider.resolution,
            self.frameProvider.fps,
            self.inputVideoThread.fpsCounter.averageFps,
            self.frameProvider.currentFrame,
        )

    def updateView(self, dt):
        self.view.ids['overlappingView'].ids['playerView'].on_frame(self.inputVideoThread.currentFrame)
        self.updateStatusBar()

    def setState(self, state: State):
        self.state = state

        if self.state == State.Stopped:
            self.view.ids['topBar'].title = '<= Select Wyze Folder'

            self.view.ids['overlappingView'].ids['recordButton'].disabled = True
            self.view.ids['overlappingView'].ids['playerView'].clear()
            self.view.ids['overlappingView'].ids['playerView'].disabled = True

            self.view.ids['statusBar'].disabled = True
            self.view.ids['statusBar'].reset()

        elif self.state == State.Playing:
            self.view.ids['topBar'].title = self.frameProvider.path

            self.view.ids['overlappingView'].ids['recordButton'].text = 'RECORD'
            self.view.ids['overlappingView'].ids['recordButton'].disabled = False
            self.view.ids['overlappingView'].ids['recordButton'].state = 'normal'

            self.view.ids['overlappingView'].ids['controlsView'].disabled = False
            self.view.ids['overlappingView'].ids['controlsView'].acceleratedLength = datetime(1900, 1, 1) + self.frameProvider.length / self.acceleration
            self.view.ids['overlappingView'].ids['controlsView'].frameStep = self.frameProvider.frameInterval * self.acceleration

            self.view.ids['overlappingView'].ids['playerView'].disabled = False

            self.view.ids['statusBar'].disabled = False

        elif self.state == State.Recording:
            self.view.ids['overlappingView'].ids['controlsView'].disabled = True
            self.view.ids['overlappingView'].ids['recordButton'].text = 'STOP'
            self.view.ids['overlappingView'].ids['playerView'].disabled = True

            self.view.ids['statusBar'].disabled = False

    def start(self, *args):
        self.frameProvider.videoEndedCallback = self.pauseAndGoToBegin
        self.setState(State.Playing)
        if self.updateViewEvent is not None:
            self.updateViewEvent.cancel()
        self.updateViewEvent = Clock.schedule_interval(self.updateView, 1 / 20)
        self.inputVideoThread.play()
        self.view.ids['overlappingView'].ids['playerView'].on_play()

    def pause(self, *agrs):
        if self.updateViewEvent is not None:
            self.updateViewEvent.cancel()
        self.view.ids['overlappingView'].ids['playerView'].on_pause()
        self.inputVideoThread.pause()

    def pauseAndGoToBegin(self, *args):
        self.inputVideoThread.setStopped()
        self.pause()
        self.seek(0)

    def seek(self, position: float):
        self.inputVideoThread.seek(position)
        Clock.schedule_once(self.updateView, 0)

    def stop(self, *args):
        if self.updateViewEvent is not None:
            self.updateViewEvent.cancel()
        self.inputVideoThread.stop()
        self.view.ids['overlappingView'].ids['playerView'].on_pause()

        if self.frameProvider:
            self.frameProvider.closeFiles()
        self.frameProvider = None

        if self.recorder:
            self.recorder.closeFile()
        self.recorder = None

        self.setState(State.Stopped)

    def inputFolderDialog(self) -> Optional[str]:
        inputFolderPath = askdirectory(
            initialdir=self.config.get('controls', 'inputFolder'))

        if not inputFolderPath:
            return inputFolderPath

        self.config.set('controls', 'inputFolder', inputFolderPath)
        return inputFolderPath

    def selectFolder(self):
        inputFolderPath = self.inputFolderDialog()
        if not inputFolderPath:
            return

        self.stop()
        self.loadingFilesDialog.open()

        self.frameProvider = FileList(
                path=inputFolderPath,
                fileLoadedCallback=self.loadingFilesDialog.updateProgress,
                createdCallback=self.filesLoaded)

        self.loadingFilesThread = Thread(target=self.frameProvider.loadFiles)
        self.loadingFilesThread.start()

    def filesLoaded(self):
        self.inputVideoThread.frameProvider = self.frameProvider
        Clock.schedule_once(self.start, 0)

    def cancelLoadingFiles(self):
        self.frameProvider.loadingFiles = False
        self.loadingFilesDialog.dismiss()

    def playPause(self):
        if self.state == State.Playing:
            if self.inputVideoThread.isPlaying():
                self.pause()
                self.view.ids['overlappingView'].ids['playerView'].on_pause()
            else:
                self.start()
                self.view.ids['overlappingView'].ids['playerView'].on_play()

    def forward(self):
        if self.state == State.Playing:
            self.seek(min(1, self.frameProvider.position + 0.02))

    def backward(self):
        if self.state == State.Playing:
            self.seek(max(0, self.frameProvider.position - 0.02))

    def startRecording(self):
        self.inputVideoThread.stop()

        self.frameProvider.videoEndedCallback = lambda: Clock.schedule_once(self.stopRecording, 0)
        self.recorder = self.factory.getRecorder(
            self.frameProvider.resolution,
            self.frameProvider.fps)

        if self.updateViewEvent is not None:
            self.updateViewEvent.cancel()
        self.updateViewEvent = Clock.schedule_interval(self.updateView, 1 / 10)

        self.setState(State.Recording)
        self.inputVideoThread.recorder = self.recorder
        self.view.ids['overlappingView'].ids['playerView'].on_play()
        self.inputVideoThread.record()

    def stopRecording(self, *args):
        self.inputVideoThread.pause()

        self.recordedDialog.open(
            self.recorder.path,
            str(self.recorder.length).split('.')[0]
        )

        if self.recorder:
            self.recorder.closeFile()
        self.recorder = None
        self.inputVideoThread.recorder = None

        if self.updateViewEvent is not None:
            self.updateViewEvent.cancel()

        self.frameProvider.videoEndedCallback = self.pauseAndGoToBegin
        self.view.ids['overlappingView'].ids['playerView'].on_pause()
        self.setState(State.Playing)

    def showAbout(self):
        self.aboutDialog.show()
